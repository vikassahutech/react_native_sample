-------------------------CREACT NEW REACT NATIVE APP-------------------------

react-native init react_native_assignment_sample (app-name)
In terminal go to home page -> cd  react_native_assignment_sample
For navigation install - > npm install @react-navigation/native @react-navigation/stack 
For navigation addition installs -> npm install react-native-reanimated react-native-gesture-handler react-native-screens react-native-safe-area-context @react-native-community/masked-view
Install simple radio button for filters -> npm i react-native-simple-radio-button
To run app in android  run command -> npm react-native run-android
Used Resources Link - 
https://www.npmjs.com/package/react-native-simple-radio-button
https://reactnative.dev/docs/navigation




-------------------------GENRATE An APK-------------------------

Open the terminal and go to Desktop

Run command -> keytool -genkeypair -v -keystore my-upload-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000

Enter the required data I will recommend to enter every where techment except 2 digit country code (91)

After the above process is completed, you will be able to find a Keystore file on your desktop with named my-upload-key.keystore.

Open  reactinative-home/android/gradle.properties  file in your project and copy the below code 

        MYAPP_UPLOAD_STORE_FILE=my-upload-key.keystore
        MYAPP_UPLOAD_KEY_ALIAS=my-key-alias
        MYAPP_UPLOAD_STORE_PASSWORD=techment
        MYAPP_UPLOAD_KEY_PASSWORD=techment
 Open   reactinative-home/android/app/build.gradle  file in your project and copy the below code   (Note -  do not delete already existing code )
        signingConfigs {
            release {
                if (project.hasProperty('MYAPP_UPLOAD_STORE_FILE')) {
                    storeFile file(MYAPP_UPLOAD_STORE_FILE)
                    storePassword MYAPP_UPLOAD_STORE_PASSWORD
                    keyAlias MYAPP_UPLOAD_KEY_ALIAS
                    keyPassword MYAPP_UPLOAD_KEY_PASSWORD
                }
            }
        }
        buildTypes {
            release {
                    // please comment signingConfig signingConfigs.debug
                      signingConfig signingConfigs.release 
            }
    }
    
Copy that keystore file from desktop in location reactinative-home/android/app

Open the terminal and go to  reactinative-home/android  and run command -->gradlew bundleRelease 

When above command executed please go and check for apk file with name app-releas.apk in path   reactinative-home/android/app/build/outputs/apk/release.

Share the app-release.apk file wherever you want

Please visit below link for more information 
    https://reactnative.dev/docs/signed-apk-android 
    https://www.decoide.org/react-native/docs/signed-apk-android.html




You can also check in Google Sheet above instructions - https://docs.google.com/document/d/1yXYzROBHEo4pXSObcS_tg7G_S0JJ0ak98cmxoR8SVYk/edit?usp=sharing