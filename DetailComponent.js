/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

class DetailComponent extends React.PureComponent{
  constructor(props){
    super(props);
    this.state={data :""}
  }

  componentDidMount(){
    if(this.props.route.params.dataItem!=undefined){
      this.setState({
        data:this.props.route.params.dataItem,
      })
    }
  }

  render(){
    return(
      <View 
      style={{
        width: '100%', flexDirection: 'column',
        alignSelf: 'center'
      }}
      >
        <Text style={{
          color: '#000',
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 7,
          fontSize: 18,
        }}>{"Title : " + this.state.data.title}</Text>

        <Text style={{
          color: '#000',
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 7,
          fontSize: 18,
        }}>{"Author - " +  this.state.data.author}</Text>
        <Text style={{
          color: '#000',
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 7,
          fontSize: 18,
        }}>{"URL - " +  this.state.data.url}</Text>
        <Text style={{
          color: '#000',
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 7,
          fontSize: 18,
        }}>{"Created At - " +  this.state.data.created_at}</Text>
      </View>
    );
  }
}
export default DetailComponent;