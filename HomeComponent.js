/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
} from 'react-native';

import React from 'react';
import { FlatList, TouchableOpacity, TextInput } from 'react-native-gesture-handler';
import RadioForm from 'react-native-simple-radio-button';



class HomeComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      hitList: [],
      hitListBackup: [],
      pageNumber: 1,
      orderAsc: true,
      searchText: "",
      searchByJSONkey: "title",
      filterByJSONKey: "title",
    };
  }

  async componentDidMount() {
    this.getDataFromAPI(1);
    this.initializeInterval();
  }

  onFilterClick=()=>{ 
    this.handleSort(this.state.filterByJSONKey);
  }

  onSearchTextChanges = (text) => {
    if(text.trim()){
      this.setState({searchText: text},()=>{
        this.search(text, this.state.searchByJSONkey);
      });
    }
  }

  onSearchRadioButtonClick = (value) => {
    this.setState({ searchByJSONkey: value},()=>{
    });
  }

  onFilterRadioButtonClick = (value) => {
    this.setState({filterByJSONKey:value});
  }

  handleSort(sortedBy) {
    let copyOfHitList = this.state.hitListBackup.slice();
    const sortedList = copyOfHitList.sort((a, b) => {
      var obj1 = a[sortedBy].toUpperCase(); // ignore upper and lowercase
      var obj2 = b[sortedBy].toUpperCase(); // ignore upper and lowercase
      if (sortedBy === "title") {
        obj1 = a.title.toUpperCase(); // ignore upper and lowercase
        obj2 = b.title.toUpperCase(); // ignore upper and lowercase
      } else {
        obj1 = a.created_at.toUpperCase(); // ignore upper and lowercase
        obj2 = b.created_at.toUpperCase(); // ignore upper and lowercase
      }

      if (this.state.orderAsc) {
        if (obj1 < obj2) {
          return -1;
        } else {
          return 1;
        }
      } else {
        if (obj1 > obj2) {
          return -1;
        } else
          return 1
      }
    });
    this.setState({ orderAsc: !this.state.orderAsc });
    this.setState({ hitList: sortedList });
  }
 
  search(textToBeSearched, searchType) {
    let copyOfDataList = this.state.hitListBackup.slice();
    var filteredArray = copyOfDataList.filter((item) => {
      if (item[searchType] !== null) {
        return item[searchType].toLowerCase().search(
          textToBeSearched.toLowerCase()) !== -1;
      }
    });
    this.setState({ hitList: filteredArray });
  }


  componentWillUnmount() {
    this.clearInitializedInterval();
  }
  getDataFromAPI(pageNumber) {
    return fetch('https://hn.algolia.com/api/v1/search_by_date?tags=story&page=' + pageNumber)
      .then((response) => response.json())
      .then((response) => {
        var data = this.state.hitListBackup.concat(response.hits);
        this.setState({
          hitList: data,
          hitListBackup: data,
        });
        return response;
      })
      .catch((error) => {
        console.error(error);
      });
  }
  initializeInterval = () => {
    this.setAPIInterval = setInterval(() => {
      this.setState({
        pageNumber: this.state.pageNumber + 1,
      }, () => {
        this.getDataFromAPI(this.state.pageNumber);
      });
    }, 10000);
  }

  clearInitializedInterval = () => {
    clearInterval(this.setAPIInterval);
  }

  onListItemClick = (item) => {
    this.props.navigation.navigate('Details', { dataItem: item });
  }
  
  onScrollEnd = () => {
    this.clearInitializedInterval();
    this.setState({
      pageNumber: this.state.pageNumber + 1
    }, () => {
      this.getDataFromAPI(this.state.pageNumber);
      this.initializeInterval();
    })
  }
 


  renderItemRows = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => this.onListItemClick(item)}
        style={{
          width: '100%', flexDirection: 'column',
          alignSelf: 'center'
        }}
      >
        <Text style={{
          color: '#fff',
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 7,
          fontSize: 18,
        }}>{"Title : " + item.title}</Text>

        <Text style={{
          color: '#fff',
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 7,
          fontSize: 18,
        }}>{"Author - " + item.author}</Text>
        <Text style={{
          color: '#fff',
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 7,
          fontSize: 18,
        }}>{"URL - " + item.url}</Text>
        <Text style={{
          color: '#fff',
          paddingLeft: 10,
          paddingRight: 10,
          paddingTop: 7,
          fontSize: 18,
        }}>{"Created At - " + item.created_at}</Text>
        <View style={{
          backgroundColor: '#fff',
          marginBottom: 10,
          marginTop: 10,
          height: 1
        }}>
        </View>
      </TouchableOpacity>
    );
  }

 
  render() {
    var radio_props_filter = [
      { label: 'Title', value: "title" },
      { label: 'Created At', value: "created_at" }
    ];

    var radio_props_search = [
      { label: 'Title', value: "title" },
      { label: 'URL', value: "url" },
      { label: 'Author', value: "author" }
    ];
    return (
      <>
        <View style={{ flex: 1 }}>
          <Text style={{ fontSize:18, padding:5, alignSelf:'center'}}> Page Number {this.state.pageNumber}</Text>
          <View style={{ flexDirection: 'row',  alignItems: 'center' }}>
            <RadioForm
              buttonSize={17}
              buttonOuterSize={20}
              borderWidth={1}
              labelStyle={{ fontSize: 14, color: '#000', paddingRight: 10 }}
              radio_props={radio_props_filter}
              formHorizontal={true}
              buttonColor={'#2196f3'}
              onPress={(value) => { this.onFilterRadioButtonClick(value) }}
            />
            <Button title="Filter" onPress={()=>{this.onFilterClick()}}></Button>

          </View>

          <View style={{ flexDirection: 'row',  alignItems: 'center' }}>
            <RadioForm
              buttonSize={17}
              buttonOuterSize={20}
              borderWidth={1}
              labelStyle={{ fontSize: 14, color: '#000', paddingRight: 10 }}
              radio_props={radio_props_search}
              formHorizontal={true}
              buttonColor={'#2196f3'}
              onPress={(value) => this.onSearchRadioButtonClick(value) }
            />
            <TextInput
              backgroundColor="#fafafa"
              style={{ height: 40 ,width:'100%'}}
              placeholder="Search Here.."
              onChangeText={text =>{this.onSearchTextChanges(text)}}
            />
          </View>

          <View style={{
            flex: 1,
            backgroundColor: "#2980b9",
            justifyContent: 'center'
          }}>
            <StatusBar barStyle="light-content" />
            <SafeAreaView>
              <ScrollView
                onMomentumScrollEnd={this.onScrollEnd}
                contentInsetAdjustmentBehavior="automatic">
                <FlatList
                  // onMomentumScrollEnd={this.onScrollEnd}
                  keyExtractor={(item, index) => index}
                  ref={ref => this.flatList = ref}
                  style={{ flex: 1, width: '100%' }}
                  data={this.state.hitList}
                  renderItem={this.renderItemRows}
                >

                </FlatList>
              </ScrollView>
            </SafeAreaView>
          </View>
        </View>
      </>
    );
  }
}

export default HomeComponent;

